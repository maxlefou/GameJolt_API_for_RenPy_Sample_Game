﻿define config.name = _("GameJolt API Sample 2")
define gui.show_name = True
define config.version = "2.0"
define gui.about = _("This is a sample that lets you see how you can use Game Jolt trophies and scoring in your Ren'Py games. It uses the py_gjapi.rpy file. Go to this page to know more about this file and know how to use Game Jolt API in Ren'Py: {a=https://github.com/maxlefou/gjapi_renpy}https://github.com/maxlefou/gjapi_renpy{/a}")
define build.name = "GJAPI2"
define config.has_sound = True
define config.has_music = True
define config.has_voice = True
define config.enter_transition = dissolve
define config.exit_transition = dissolve
define config.after_load_transition = None
define config.end_game_transition = None
define config.window = "auto"
define config.window_show_transition = Dissolve(.2)
define config.window_hide_transition = Dissolve(.2)
default preferences.text_cps = 0
default preferences.afm_time = 15
define config.save_directory = "GameJoltAPISample2-1489827249"
define config.window_icon = "gui/gjicon.png"

init python:
    
    # This needs to be here to remove that hideous default checkered background. I can't believe we still need to do this.
    config.layers.insert(0, 'background')
    
    build.classify('**~', None)
    build.classify('**.bak', None)
    build.classify('**/.**', None)
    build.classify('**/#**', None)
    build.classify('**/thumbs.db', None)

    build.documentation('*.html')
    build.documentation('*.txt')
    
    ########################################################################
    # GAMEJOLT API

    # Define here the keys of the game. Don't forget this !!!
    persistent.GJGameKey = "127112"
    persistent.GJPrivateKey = "2c5425f2ed988824126d7d30d01da133"
    
    # Persistent values so they can be saved for later.
    persistent.GJusername
    persistent.GJtoken
    if persistent.GJstatus is None:
        persistent.GJstatus = False
        
    
    ########################################################################
