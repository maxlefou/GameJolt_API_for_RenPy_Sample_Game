# Game Jolt API for RenPy - Sample game

[![Licence](https://img.shields.io/badge/license-GNU%20LGPLv3-green.svg)](https://www.gnu.org/licenses/lgpl-3.0.en.html)
[![Ren'Py](https://img.shields.io/badge/renpy-7.x-blue.svg)](https://renpy.org)

This is a sample that lets you see how you can use Game Jolt trophies and scoring in your Ren'Py games. It uses the py_gjapi.rpy file. Go to [this page](https://gitlab.com/maxlefou/gjapi_renpy) to know more about this file and know how to use Game Jolt API in Ren'Py.

To test the script, create a new project (legacy or new gui) then take either of the corresponding "game" folders and put it in your project's folder. Then, launch it from the launcher. [You can also go there](http://gamejolt.com/games/game-jolt-api-for-ren-py-sample-game/127112) to get a compiled (but outdated) version of this game.

#### This program is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License. If you don't want to read all the license but need to mainly know what it means, go to the following page: https://tldrlegal.com/license/gnu-lesser-general-public-license-v3-(lgpl-3)
